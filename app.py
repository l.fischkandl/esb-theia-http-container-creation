import docker
from flask import Flask,redirect, request
import json
import sqlite3

def createContainer():
    client = docker.from_env()
    container = client.containers.run("esb-theia",detach=True, ports= {'3000/tcp': None}, mem_limit="1g", network="esb-digital-boardroom_default")
    container.logs()
    container.reload()

    port = container.ports["3000/tcp"][0]["HostPort"]
    return port

app = Flask(__name__)

def writeUser(ip,port):
    conn = sqlite3.connect("test.db")
    cur = conn.cursor()
    cur.execute("INSERT INTO users VALUES(?,?)",(ip,port))
    conn.commit()

def checkUser(ip):
    conn = sqlite3.connect("test.db")
    cur = conn.cursor()
    ans = cur.execute("SELECT * FROM users WHERE ip = ?",(ip,)).fetchall()
    if(ans==[]):
        ans = ans
    else: 
        ans = ans[0][1]
    return ans


@app.route("/")
def test():
  
    url = request.url  
    ip = request.remote_addr

    check = checkUser(ip)

    if(check==[]):
        container = createContainer()
        url = url[:-1] +":"+str(container)
        writeUser(ip,container)
    else:
        url = url[:-1] +":"+str(check)

    return redirect(url, code=301)


if __name__ == '__main__':

    app.run(host='0.0.0.0', port=80)