# Server Requirements 
- python2
- pip 
- docker: [Docker Install Script](https://github.com/docker/docker-install)

# Step 1: Install Python Requirements 
```
pip install -r requirements.txt
```
# Step 2: Init DB 
```
python init_db.py
```
# Step 3: Start Server
```
python app.py
```
# Alle Docker Container Stoppen und Löschen
```
docker stop $(docker ps -aq)
docker rm $(docker ps -aq)
```